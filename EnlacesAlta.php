<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {
          $id = recoge("id");
          $nombre = recoge("nombre");
          $url = recoge("url");
          $tipoenlace = recoge("tipoenlace");


          $enlace = new Enlaces($id, $nombre, $url, $tipoenlace);
          return $enlace;
        }

        //***************************
        //* Main
        //***************************

        $enlace = leer();
        if ($enlace->getId() != "" && $enlace->getNombre() != "") {
          $fichero = new Ficheros();
          $fichero->grabarEnlace($enlace);
          //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
          //echo "Error: Campos vacios" . "<br>";
        }

        header('Location: ./EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
