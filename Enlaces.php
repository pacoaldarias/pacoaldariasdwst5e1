<?php

class Enlaces {

//Propiedades
  private $id;
  private $nombre;
  private $url;
  private $tipoenlace;

//Constructor

  public function __construct($id, $nombre, $url, $tipoenlace) {
    $this->id = $id;
    $this->nombre = $nombre;
    $this->url = $url;
    $this->tipoenlace = $tipoenlace;
  }

//Metodos
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
  }

  public function getUrl() {
    return $this->url;
  }

  public function setUrl($url) {
    $this->url = $url;
  }

  public function getTipoenlace() {
    return $this->tipoenlace;
  }

  public function setTipoenlace($tipoenlace) {
    $this->tipoenlace = $tipoenlace;
  }

}

?>
